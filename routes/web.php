<?php

use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\PendaftaranController;
use App\Http\Controllers\ForgetPasswordController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\pengunjung\DashboardController as PengunjungDashboardController;
use App\Http\Controllers\pengunjung\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'index'])->name('landing_page');
Route::prefix('auth')->group(function () {
    Route::post('register', [LoginController::class, 'register'])->name('register');
    Route::prefix('admin')->group(function () {
        Route::get('login', [HomeController::class, 'login'])->name('admin-login');
        // Route::post('post-register', [LoginController::class, 'postRegister'])->name('post-register');
        // Route::post('post-login', [LoginController::class, 'authenticate'])->name('post-login');
        // Route::get('logout', [LoginController::class, 'logout'])->name('user-logout');
    });
    // Route::post('post-register', [LoginController::class, 'postRegister'])->name('post-register');
    // Route::post('post-login', [LoginController::class, 'authenticate'])->name('post-login');
    // Route::get('logout', [LoginController::class, 'logout'])->name('user-logout');
});
Route::prefix('pengunjung')->group(function () {
    Route::get('dashboard', [PengunjungDashboardController::class, 'index'])->name('store_pendaftaran');
    Route::post('daftar', [HomeController::class, 'store'])->name('store_pendaftaran');
    // Route::post('save', [HomeController::class, 'store'])->name('proses_save_sekolah');
    // Route::get('langganan', [HomeController::class, 'langganan'])->name('halaman-langganan');
    // Route::post('cek', [HomeController::class, 'cek'])->name('proses_cek_domain');
    // Route::get('re_subdomain', [HomeController::class, 're_subdomain'])->name('redirect_sub_domain');
});



Route::get('admin', [DashboardController::class, 'index'])->name('admin_dashboard');
Route::prefix('admin')->group(function () {
    Route::get('dashboard', [ForgetPasswordController::class, 'forget_password'])->name('user-forget_password');
    Route::get('password', [ForgetPasswordController::class, 'forget_password'])->name('user-forget_password');
    Route::post('forget_password', [ForgetPasswordController::class, 'submitForgetPasswordForm'])->name('user-forget_form');

});
Route::get('pendaftaran', [PendaftaranController::class, 'index'])->name('admin_pendaftaran');

Route::prefix('forget')->group(function () {
    Route::get('password', [ForgetPasswordController::class, 'forget_password'])->name('user-forget_password');
    Route::post('forget_password', [ForgetPasswordController::class, 'submitForgetPasswordForm'])->name('user-forget_form');

});
Route::prefix('pendaftaran')->group(function () {
    Route::post('daftar', [HomeController::class, 'store'])->name('store_pendaftaran');
    Route::post('get_kabupaten', [HomeController::class, 'get_kabupaten'])->name('get_kabupaten-by_provinsi');
    Route::post('get_kecamatan', [HomeController::class, 'get_kecamatan'])->name('get_kecamatan-by_kabupaten');
    Route::get('get_all', [HomeController::class, 'all_data'])->name('pengunjung-data_pendaftar');
    // Route::post('save', [HomeController::class, 'store'])->name('proses_save_sekolah');
    // Route::get('langganan', [HomeController::class, 'langganan'])->name('halaman-langganan');
    // Route::post('cek', [HomeController::class, 'cek'])->name('proses_cek_domain');
    // Route::get('re_subdomain', [HomeController::class, 're_subdomain'])->name('redirect_sub_domain');
});

Route::prefix('list')->group(function () {
    Route::get('year/{year}', [PendaftaranController::class, 'list_year'])->name('data_pertahun');
    Route::post('filter', [PendaftaranController::class, 'filter_month'])->name('filter_pertahun');
    Route::post('get_id', [PendaftaranController::class, 'get_by_id'])->name('pendaftaran-get_by_id');
    Route::post('update_status', [PendaftaranController::class, 'update_status'])->name('pendaftaran-update_status');
    Route::post('detail', [PendaftaranController::class, 'detail_pendaftaran'])->name('pendaftaran-detail');
    Route::post('create', [PendaftaranController::class, 'store'])->name('pendaftaran-create');
    Route::post('edit_kabupaten', [PendaftaranController::class, 'load_kabupaten'])->name('pendaftaran-get_by_kabupaten');
    Route::post('edit_kecamatan', [PendaftaranController::class, 'load_kecamatan'])->name('pendaftaran-get_by_kecamatan');
});
