<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendaftaran extends Model
{
    use SoftDeletes;

    protected $table = 'pendaftarans';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nama',
        'perkara',
        'alamat',
        'jenkel',
        'hp',
        'email',
        'tanggal_jadi',
        'created_at',
        'tahun_perkara',
        'nomor_perkara',
        'id_provinsi',
        'id_kabupaten',
        'id_kecamatan',
        'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 2);
    }

}
