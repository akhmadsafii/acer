<?php

namespace App\Providers;

use App\Pendaftaran;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View()->composer('content.admin.v_menu', function ($view) {
            $tahun = DB::select('
        select
            YEAR(t.created_at) AS YEAR
        FROM
            pendaftarans t
        GROUP BY YEAR(t.created_at) DESC;');
            $view->with(['tahun' => $tahun]);
        });
    }
}
