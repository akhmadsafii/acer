<?php


namespace App\Helpers;
use Carbon\Carbon;

class Help
{
    
    public static function status($param)
    {
        switch ($param) {
            case 1:
                return 'Aktif';
                break;
            case 2:
                return 'Tidak Aktif';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function gender($param)
    {
        switch ($param) {
            case 1:
                return 'Laki-laki';
                break;
            case 2:
                return 'Perempuan';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function getStNikah($param)
    {

        switch ($param) {
            case 1:
                return 'Belum Menikah';
                break;
            case 2:
                return 'Menikah';
                break;
            case 3:
                return 'Janda';
                break;
            case 4:
                return 'Duda';
                break;

            default:
                return 'Error';
                break;
        }
    }
   
    public static function getKelas($param)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($param > 0) {
            foreach ($map as $roman => $int) {
                if($param >= $int) {
                    $param -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public static function getTanggal($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F Y');
        return $value;
    }
    
    public static function timeHuman($param)
    {
        $value = Carbon::parse($param)->diffForHumans();
        return $value;
    }
    
    public static function getMonthYear($param)
    {
        $value = Carbon::parse($param)->translatedFormat('F Y');
        return $value;
    }
    
    public static function getDayMonth($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F');
        return $value;
    }
    
    public static function getHoursMinute($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F Y  H:i');
        return $value;
    }
    
    public static function getDay($param)
    {
        $value = Carbon::parse($param)->translatedFormat('l, d F Y');
        return $value;
    }
    
    public static function getTanggalLengkap($param)
    {
        $value = Carbon::parse($param)->translatedFormat('l, d F Y  H:i');
        return $value;
    }

    public static function check_and_make_dir($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
    }

    public static function pagination($data, $request)
    {
        return [
            "totalCount" => $data->total(),
            "pageSize" => $data->perPage(),
            "currentPage" => $data->currentPage(),
            "totalPages" => $data->lastPage(),
            "previousPage" => is_null($data->previousPageUrl()) ? false : $data->appends($request->all())->previousPageUrl(),
            "nextPage" => is_null($data->nextPageUrl()) ? false : $data->appends($request->all())->nextPageUrl(),
        ];
    }
}
