<?php

namespace App\Http\Controllers;

use App\Model\UserPendaftar;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        return view('content.auth.login')->with([]);
    }

    public function register(Request $request)
    {
        $customAttributes = [
            'nama' => 'Nama',
            'jenkel' => 'Jenis Kelamin',
            'email' => 'Email',
            'telepon' => 'Nomor Whatsapp',
            'no_kk' => 'Koordinat Longitude',
        ];

        $rules = [
            'nama' => 'required',
            'jenkel' => 'required',
            'telepon' => 'required',
            'no_kk' => 'required',
            'nik' => 'required|unique:user_pendaftars,nik',
            'email' => 'required|email|unique:user_pendaftars,email',
        ];

        $messages = [
            'required' => ':attribute harus diisi.',
            'max' => ':attribute tidak lebih dari :max',
            'email' => 'Format penulisan :attribute belum benar.',
            'unique' => ':attribute sudah terdaftar.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first(),
                'status' => 'error',
            ], 302);
        }
        // dd($request);
        $pendaftar           = new UserPendaftar();
        $pendaftar->nik     = $request->nik;
        $pendaftar->no_kk    = $request->no_kk;
        $pendaftar->nama    = $request->nama;
        $pendaftar->telepon    = $request->telepon;
        $pendaftar->email    = $request->email;
        $pendaftar->jenkel    = $request->jenkel;
        $pendaftar->password    = 12345;
        $pendaftar->first_password    = 12345;
        $pendaftar->save();
        // dd($pendaftar);
        return response()->json([
            'message' => 'Pendaftaran berhasil',
            'status' => 'success'
        ], 200);
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('admin_dashboard');
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    public function postRegister(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();

        $check = $this->create($data);

        return redirect()->to("admin_dashboard")->withSuccess('Great! You have Successfully loggedin');
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect()->route('login');
    }
}
