<?php

namespace App\Http\Controllers\pengunjung;

use App\Http\Controllers\Controller;
use App\Pendaftaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Indonesia;
use DataTables;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('content.landing.v_beranda')->with([]);
    }

    public function login(){
        return view('content.landing.v_login')->with([]);
    }

    public function store(Request $request)
    {
        $pendaftaran           = new Pendaftaran();
        $pendaftaran->nama     = $request->nama;
        $pendaftaran->email    = $request->email;
        $pendaftaran->alamat    = $request->alamat;
        $pendaftaran->nomor_perkara    = $request->nomor_perkara;
        $pendaftaran->tahun_perkara    = $request->tahun_perkara;
        $pendaftaran->jenkel    = $request->jenkel;
        $pendaftaran->id_provinsi   = $request->id_provinsi;
        $pendaftaran->id_kabupaten   = $request->id_kabupaten;
        $pendaftaran->id_kecamatan   = $request->id_kecamatan;
        $pendaftaran->hp   = $request->hp;
        $pendaftaran->tanggal_jadi   = $request->tanggal_jadi;
        $pendaftaran->status   = 0;
        $pendaftaran->save();
        // dd($pendaftaran);
        return back()->with('success', 'Selamat, pendaftaran anda berhasil ditambahkan.');
    }

    public function get_kabupaten(Request $request)
    {
        // dd($request);
        $kabupaten = Indonesia::findProvince($request->id, ['cities']);
        return response()->json($kabupaten);
    }

    public function get_kecamatan(Request $request)
    {
        // dd($request);
        $kecamatan = Indonesia::findCity($request->id, ['districts']);
        return response()->json($kecamatan);
    }

    public function all_data()
    {
        $pendaftaran =
        DB::table('pendaftarans')
        ->leftJoin('indonesia_provinces', 'pendaftarans.id_provinsi', '=', 'indonesia_provinces.id')
        ->leftJoin('indonesia_cities', 'pendaftarans.id_kabupaten', '=', 'indonesia_cities.id')
        ->leftJoin('indonesia_districts', 'pendaftarans.id_kecamatan', '=', 'indonesia_districts.id')
        ->select('pendaftarans.*', 'indonesia_provinces.name as nama_provinsi', 'indonesia_cities.name as nama_kabupaten', 'indonesia_districts.name as nama_kecamatan')
        ->where('status', '!=',  0);
        return  Datatables::of($pendaftaran)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
