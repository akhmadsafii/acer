<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Pendaftaran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Helpers\Help;
use Illuminate\Support\Facades\Validator;
use Indonesia;

class PendaftaranController extends Controller
{
    public function index(Request $request)
    {
        $provinsi = Indonesia::allProvinces();
        $pendaftaran = Pendaftaran::select('*');
        if ($request->ajax()) {
            $table =  Datatables::of($pendaftaran)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-primary btn-xs detail"><i class="mdi mdi-eye"></i></a> &nbsp;&nbsp;';
                    $btn .= '<a href="javascript:void(0)"  data-id="' . $row->id . '"class="btn btn-success btn-xs edit"><i class="mdi mdi-pencil"></i></a>';
                    return $btn;
                });
            $table->editColumn('status', function ($rows) {
                if ($rows->status == 0) {
                    return '<a href="javascript:void(0)" class="btn btn-danger btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Pending</a>';
                } elseif ($rows->status == 1) {
                    return '<a href="javascript:void(0)" class="btn btn-warning btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Belum Jadi</a>';
                } else {
                    return '<a href="javascript:void(0)" class="btn btn-success btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Sudah Jadi</a>';
                }
            });
            $table->rawColumns(['action', 'status']);
            return $table->make(true);
        }
        return view('content.admin.pendaftaran.v_pendaftaran')->with(['provinsi' => $provinsi]);
    }

    public function list_year(Request $request, $tahun)
    {
        $provinsi = Indonesia::allProvinces();
        // dd($provinsi);
        $bulan = Pendaftaran::select(
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
            DB::raw('max(created_at) as createdAt')
        )
            ->where(DB::raw('YEAR(created_at)'), "=", $tahun)
            ->orderBy('createdAt', 'desc')
            ->groupBy('months')
            ->get();
        $pendaftaran = Pendaftaran::whereYear('created_at', '=', $tahun);
        if ($request->ajax()) {
            return  Datatables::of($pendaftaran)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('content.admin.pendaftaran.v_tahun_pendaftaran')->with(['bulan' => $bulan, 'provinsi' => $provinsi]);
    }

    public function filter_month(Request $request)
    {
        $month = date('m', strtotime($request->bulan));
        $year = date('Y', strtotime($request->bulan));
        $pendaftaran = DB::table("pendaftarans")
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month);
        $table =  Datatables::of($pendaftaran)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-primary btn-xs detail"><i class="mdi mdi-eye"></i></a> &nbsp;&nbsp;';
                $btn .= '<a href="javascript:void(0)"  data-id="' . $row->id . '"class="btn btn-success btn-xs edit"><i class="mdi mdi-pencil"></i></a>';
                return $btn;
            });
        $table->editColumn('status', function ($rows) {
            if ($rows->status == 0) {
                return '<a href="javascript:void(0)" class="btn btn-danger btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Pending</a>';
            } elseif ($rows->status == 1) {
                return '<a href="javascript:void(0)" class="btn btn-warning btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Belum Jadi</a>';
            } else {
                return '<a href="javascript:void(0)" class="btn btn-success btn-sm changeStatus" data-id="' . $rows->id . '"  style="color: #fff"><i class="fas fa-info-circle"></i> Sudah Jadi</a>';
            }
        });
        $table->rawColumns(['action', 'status']);
        return $table->make(true);
    }

    public function get_by_id(Request $request)
    {
        $pendaftaran = Pendaftaran::find($request->id);
        $pendaftaran['created'] = date('Y-m-d', strtotime($pendaftaran->created_at));
        return response()->json($pendaftaran);
    }

    public function update_status(Request $request)
    {
        $update = Pendaftaran::find($request->id);
        $update->status = $request->status;
        if ($request->status == 2) {
            $update->tanggal_jadi = $request->tanggal_jadi;
        } else {
            $update->tanggal_jadi = null;
        }
        $update->save();
        // dd($update);
        return response()->json("status berhasil diupdate");
    }

    public function detail_pendaftaran(Request $request)
    {
        $data = DB::table('pendaftarans')
            ->leftJoin('indonesia_provinces', 'pendaftarans.id_provinsi', '=', 'indonesia_provinces.id')
            ->leftJoin('indonesia_cities', 'pendaftarans.id_kabupaten', '=', 'indonesia_cities.id')
            ->leftJoin('indonesia_districts', 'pendaftarans.id_kecamatan', '=', 'indonesia_districts.id')
            ->select('pendaftarans.*', 'indonesia_provinces.name as nama_provinsi', 'indonesia_cities.name as nama_kabupaten', 'indonesia_districts.name as nama_kecamatan')
            ->where('pendaftarans.id', $request->id)->first();
        // dd($data);
        $status = '';
        if ($data->status == 0) {
            $status = '<div class="badge badge-opacity-danger me-3">Pending</div>';
        } else if ($data->status == 1) {
            $status = '<div class="badge badge-opacity-warning me-3">Belum Jadi</div>';
        } else {
            $status = '<div class="badge badge-opacity-success me-3">Sudah Jadi</div>';
        }
        $html = '
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Status</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $status . '</p>
            </div>

            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Nama</b></h6>
                <p rel="field-nama" class="mr-t-0">' . $data->nama . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Nomer Perkara</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $data->nomor_perkara . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Tahun Perkara</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $data->tahun_perkara . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Jenis Kelamin</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $data->jenkel . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Email</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $data->email . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Telepon</b></h6>
                <p rel="field-tgl-bayar" class="mr-t-0">' . $data->hp . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Tanggal Pengajuan</b></h6>
                <p rel="field-kelas" class="mr-t-0">' . Help::getTanggal($data->created_at) . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Tanggal Jadi</b></h6>
                <p rel="field-jurusan" class="mr-t-0">' . Help::getTanggal($data->tanggal_jadi) . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Provinsi</b></h6>
                <p rel="field-rombel" class="mr-t-0">' . $data->nama_provinsi . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Kabupaten</b></h6>
                <p rel="field-rombel" class="mr-t-0">' . $data->nama_kabupaten . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-uppercase"><b>Kecamatan</b></h6>
                <p rel="field-rombel" class="mr-t-0">' . $data->nama_kecamatan . '</p>
            </div>
            <div class="col-md-12">
                <h6 class="text-uppercase"><b>Alamat</b></h6>
                <p rel="field-rombel" class="mr-t-0">' . $data->alamat . '</p>
            </div>
        ';
        return response()->json($html);
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nama'      => 'required',
            'email'     => 'required|email|unique:users'
        ]);
        if ($request->created_at != null) {
            $created_at = $request->created_at;
        } else {
            $created_at = Carbon::now();
        }
        if($request->status != 2){
            $tgl_jadi = null;
        }else{
            $tgl_jadi = $request->tanggal_jadi;
        }
        $pendaftaran = Pendaftaran::firstOrNew(['id' => $request->id]);
        $pendaftaran->created_at    = $created_at;
        $pendaftaran->nama          = $request->nama;
        $pendaftaran->email    = $request->email;
        $pendaftaran->alamat    = $request->alamat;
        $pendaftaran->nomor_perkara    = $request->nomor_perkara;
        $pendaftaran->tahun_perkara    = $request->tahun_perkara;
        $pendaftaran->jenkel    = $request->jenkel;
        $pendaftaran->id_provinsi   = $request->provinsi;
        $pendaftaran->id_kabupaten   = $request->id_kabupaten;
        $pendaftaran->id_kecamatan   = $request->id_kecamatan;
        $pendaftaran->hp   = $request->telepon;
        $pendaftaran->tanggal_jadi   = $tgl_jadi;
        $pendaftaran->status   = $request->status;
        $pendaftaran->save();
        return response()->json([
            'message' => 'Pendaftaran successfully created.'
        ]);
    }

    public function load_kabupaten(Request $request)
    {
        $id_kabupaten = $request->id_kabupaten;
        $result = Indonesia::findProvince($request->id_provinsi, ['cities']);
        $output = '';
        foreach ($result['cities'] as $brand) {
            // dd($brand);
            $brand_id = $brand['id'];
            $brand_name = $brand['name'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id_kabupaten) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function load_kecamatan(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;
        $result = Indonesia::findCity($request->id_kabupaten, ['districts']);
        $output = '';
        foreach ($result['districts'] as $brand) {
            // dd($brand);
            $brand_id = $brand['id'];
            $brand_name = $brand['name'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id_kecamatan) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }
}
