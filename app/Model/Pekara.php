<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pekara extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function pendaftar()
    {
        return $this->belongsTo(UserPendaftar::class, 'id_user_pendaftar', 'id');
    }

    // public function sekolah()
    // {
    //     return $this->belongsTo(Sekolah::class, 'id_sekolah', 'id');
    // }

    // public function details()
    // {
    //     return $this->hasMany(KepegawaianDetailGaji::class, 'id_gaji', 'id');
    // }
}
