<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AktaCerai extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    // public function pegawai()
    // {
    //     return $this->belongsTo(KepegawaianPegawai::class, 'id_pegawai', 'id');
    // }

    // public function sekolah()
    // {
    //     return $this->belongsTo(Sekolah::class, 'id_sekolah', 'id');
    // }

    // public function details()
    // {
    //     return $this->hasMany(KepegawaianDetailGaji::class, 'id_gaji', 'id');
    // }
}
