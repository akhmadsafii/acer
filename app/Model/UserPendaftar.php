<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPendaftar extends Model
{
    use SoftDeletes;

    protected $table = 'user_pendaftars';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    protected $casts = [
        'status' => 'integer'
    ];

    protected $hidden = ['password'];
}
