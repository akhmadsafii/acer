<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Laravolt\Indonesia\Seeds\CitiesSeeder;
use Laravolt\Indonesia\Seeds\VillagesSeeder;
use Laravolt\Indonesia\Seeds\DistrictsSeeder;
use Laravolt\Indonesia\Seeds\ProvincesSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(
            [
                ProvincesSeeder::class,
                CitiesSeeder::class,
                DistrictsSeeder::class,
                VillagesSeeder::class,
            ]
        );

        $faker = Faker::create();

        $gender = $faker->randomElement(['male', 'female']);

    	for($i = 1; $i <= 500; $i++){
            DB::table('pendaftarans')->insert([
                'nama' => $faker->name($gender),
                'email' => $faker->email,
                'nomor_perkara' => $faker->numerify('###-###-####'),
                'tahun_perkara' => rand(2015,2021),
                'alamat' => $faker->text,
                'jenkel' => $gender,
                'id_provinsi' => rand(11,94),
                'id_kabupaten' => rand(1101,9471),
                'id_kecamatan' => rand(1101010,9471040),
                'status' => rand(0,2),
                'hp' => $faker->phoneNumber,
                'tanggal_jadi' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'created_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
            ]);
        }

    }
}
