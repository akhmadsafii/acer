@extends('content.admin/main')
@section('content')

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Pendaftar</h4>
                    <p class="card-description">
                        Add class <code>.table-bordered</code>
                    </p>
                    <form action="javascript:void(0)" id="filterDate">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Pilih Bulan :</strong></label>
                                    <select id="bulan" name="bulan" class="form-control">
                                        @foreach ($bulan as $bln)
                                            <option value="{{ $bln->createdAt }}">{{ $bln->months }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Status :</strong></label>
                                    <select id="status" name="status" class="form-control">
                                        <option value="">--Select status--</option>
                                        <option value="0">Pending</option>
                                        <option value="1">Belum Jadi</option>
                                        <option value="2">Sudah Jadi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label><strong>&nbsp;</strong></label>
                                    <input type="submit" class="form-control btn btn-success" style="background: #28a745;"
                                        value="Cari">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label><strong>&nbsp;</strong></label>
                                    <input type="button" class="form-control btn btn-info" value="Tambah Data" id="btnAdd"
                                        style="background: #17a2b8;">
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>No Perkara</th>
                                <th>Email</th>
                                <th>Telepon</th>
                                <th>Status</th>
                                <th>Tanggal Jadi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <h5 class="modal-title" id="modelHeading" style="color: #fff"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row" id="resultData">
                            <div class="form-group">
                                <label for="">Nomor Perkara</label>
                                <input type="text" name="nomor_perkara" id="nomor_perkara" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" id="nama" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Tahun Perkara</label>
                                <input type="text" name="tahun_perkara" id="tahun_perkara" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenkel" id="jenkel" class="form-control">
                                    <option value="l">Laki - laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Telepon</label>
                                <input type="text" name="telepon" id="telepon" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Provinsi</label>
                                <select name="provinsi" id="provinsi" class="form-control">
                                    <option value="">-- Pilih Provinsi --</option>
                                    @foreach ($provinsi as $pr)
                                        <option value="{{ $pr->id }}">{{ $pr->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kabupaten/Kota</label>
                                <select name="id_kabupaten" id="kabupaten" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                <select name="id_kecamatan" id="kecamatan" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" class="form-control" id="alamat" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="aksiFooter">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="statusModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog">
            <form id="StatusForm" class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header text-inverse" style="background: #03a9f3;">
                        <h5 class="modal-title" id="headingStatus" style="color: #fff"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="pending" value="0">
                                    Pending
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="belum" value="1">
                                    Belum Jadi
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="sudah" value="2">
                                    Sudah Jadi
                                </label>
                            </div>
                        </div>

                        <div class="row p-2 panel-collapse collapse in"
                            style="border-radius: 15px; border: 1px solid #dadada" id="sudahJadi">
                            <div class="col-xs-12 col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="">TANGGAL JADI</label>
                                        <input type="date" name="tanggal_jadi" id="tanggal_jadi" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id_status">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnUpdate"
                            value="create">Update Status</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {

            var bulan = $('#bulan').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('filter_pertahun') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.bulan = bulan;
                    },
                },
                columns: [{
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nomor_perkara',
                        name: 'nomor_perkara'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'hp',
                        name: 'hp'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'tanggal_jadi',
                        name: 'tanggal_jadi'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#filterDate').on('submit', function(event) {
                bulan = $('#bulan').val();
                table.ajax.reload().draw();
            });

            $(document).on('click', '.detail', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#modelHeading').html('Detail Pendaftaran');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pendaftaran-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $('#aksiFooter').hide();
                        $(loader).html('<i class="mdi mdi-eye"></i>');
                        $('#resultData').html(data);
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#modelHeading').html('Edit Pendaftaran');
                $.ajax({
                    type: "POST",
                    url: "{{ route('pendaftaran-add_form') }}",
                    data: {
                        action: "update",
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $('#aksiFooter').show();
                        $(loader).html(
                            '<i class="mdi mdi-pencil"></i>');
                        $('#resultData').html(data);
                        $('#ajaxModel').modal('show');
                    }
                });

            });

            // $(document).on('click', '#btnAdd', function() {
            //     $('#modelHeading').html('Tambah Pendaftaran');
            //     $.ajax({
            //         type: "POST",
            //         url: "{{ route('pendaftaran-add_form') }}",
            //         data: {
            //             action: "add"
            //         },
            //         success: function(data) {
            //             $('#aksiFooter').show();
            //             $('#ajaxModel').modal('show');
            //         }
            //     });

            // });

            $(document).on('click', '.changeStatus', function() {
                let id = $(this).data('id');
                let loader = $(this)
                $('#form_result').html('');
                $('#headingStatus').html('Update Status');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pendaftaran-get_by_id') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        // $(loader).html(
                        //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // $(loader).html(
                        //     '<i class="fas fa-info-circle"></i> Sudah jadi');
                        $('#id_status').val(data.id);
                        if (data.status == 0) {
                            $("#pending").prop("checked", true);
                            $('#sudahJadi').collapse('hide');
                        } else if (data.status == 1) {
                            $("#belum").prop("checked", true);
                            $('#sudahJadi').collapse('hide');
                        } else {
                            $("#sudah").prop("checked", true);
                            $('#tanggal_jadi').val(data.tanggal_jadi);
                            $('#sudahJadi').collapse('show');
                        }
                        $('#statusModal').modal('show');
                    }
                });
            });

            $('#btnAdd').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Jurusan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#StatusForm').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdate").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpdate").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pendaftaran-update_status') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $.toast({
                            icon: 'success',
                            text: data,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        $('#statusModal').modal('hide');
                        var oTable = $('.data-table').dataTable();
                        oTable.fnDraw(false);
                        $('#btnUpdate').html('Update Status');
                        $("#btnUpdate").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('select[name="provinsi"]').on('change', function() {
                var id = $(this).val();
                if (IDBFactory) {
                    $.ajax({
                        url: "{{ route('get_kabupaten-by_provinsi') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        dataType: "json",
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kabupaten"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '';
                                data.cities.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .name +
                                        '</option>';

                                })
                                $('select[name="id_kabupaten"]').removeAttr('disabled');
                            }
                            $('select[name="id_kabupaten"]').html(s)
                        }
                    });
                }
            })
            $('select[name="id_kabupaten"]').on('change', function() {
                var id = $(this).val();
                if (IDBFactory) {
                    $.ajax({
                        url: "{{ route('get_kecamatan-by_kabupaten') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        dataType: "json",
                        success: function(data) {
                            console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="id_kecamatan"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '';
                                data.districts.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .name +
                                        '</option>';

                                })
                                $('select[name="id_kecamatan"]').removeAttr('disabled');
                            }
                            $('select[name="id_kecamatan"]').html(s)
                        }
                    });
                }
            })


        });
        $('#sudahJadi').collapse('hide');
        $('input[name="status"]').change(function() {
            if ($('#sudah').is(":checked")) {
                $('#sudahJadi').collapse('show');
            } else {
                $('#sudahJadi').collapse('hide');
            }
        });
    </script>
@endsection
