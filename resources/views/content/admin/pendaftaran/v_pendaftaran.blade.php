@extends('content.admin/main')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Pendaftar</h4>
                    <p class="card-description">
                        Add class <code>.table-bordered</code>
                    </p>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <button class="btn btn-info" id="btnAdd">Add Data</button>
                        </div>
                    </div>

                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>No Perkara</th>
                                <th>Email</th>
                                <th>Telepon</th>
                                <th>Status</th>
                                <th>Tanggal Jadi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <h5 class="modal-title" id="modelHeading" style="color: #fff"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="pendaftaranForm" name="pendaftaranForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row" id="resultData">
                            <div class="form-group">
                                <label for="">Created At</label>
                                <input type="date" name="created_at" id="created_at" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nomor Perkara</label>
                                <input type="text" name="id" id="id_pendaftaran">
                                <input type="text" name="nomor_perkara" id="nomor_perkara" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" id="nama" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Tahun Perkara</label>
                                <input type="text" name="tahun_perkara" id="tahun_perkara" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenkel" id="jenkel" class="form-control">
                                    <option value="l">Laki - laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Telepon</label>
                                <input type="text" name="telepon" id="telepon" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Provinsi</label>
                                <select name="provinsi" id="provinsi" class="form-control">
                                    <option value="">-- Pilih Provinsi --</option>
                                    @foreach ($provinsi as $pr)
                                        <option value="{{ $pr->id }}">{{ $pr->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kabupaten/Kota</label>
                                <select name="id_kabupaten" id="kabupaten" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                <select name="id_kecamatan" id="kecamatan" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" class="form-control" id="alamat" rows="3"></textarea>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Status</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <input type="radio" name="status" class="track-order-change" id="pending"
                                                value="0">
                                            Pending
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <input type="radio" name="status" class="track-order-change" id="belum"
                                                value="1">
                                            Belum Jadi
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <input type="radio" name="status" class="track-order-change" id="sudah"
                                                value="2">
                                            Sudah Jadi
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row p-2 panel-collapse collapse in" id="sudahJadi">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="row">
                                                    <label for="">TANGGAL JADI</label>
                                                    <input type="date" name="tanggal_jadi" id="tanggal_jadi"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="aksiFooter">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="statusModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog">
            <form id="StatusForm" class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header text-inverse" style="background: #03a9f3;">
                        <h5 class="modal-title" id="headingStatus" style="color: #fff"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="pendings" value="0">
                                    Pending
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="belums" value="1">
                                    Belum Jadi
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <input type="radio" name="status" class="track-order-change" id="sudahs" value="2">
                                    Sudah Jadi
                                </label>
                            </div>
                        </div>

                        <div class="row p-2 panel-collapse collapse in"
                            style="border-radius: 15px; border: 1px solid #dadada" id="sudahJadis">
                            <div class="col-xs-12 col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="">TANGGAL JADI</label>
                                        <input type="date" name="tanggal_jadi" id="tanggal_jadis" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id_status">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnUpdate"
                            value="create">Update Status</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nomor_perkara',
                        name: 'nomor_perkara'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'hp',
                        name: 'hp'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'tanggal_jadi',
                        name: 'tanggal_jadi'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#btnAdd').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Pendaftaran");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('select[name="provinsi"]').on('change', function() {
                var id = $(this).val();
                if (IDBFactory) {
                    $.ajax({
                        url: "{{ route('get_kabupaten-by_provinsi') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        dataType: "json",
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kabupaten"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '';
                                data.cities.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .name +
                                        '</option>';

                                })
                                $('select[name="id_kabupaten"]').removeAttr('disabled');
                            }
                            $('select[name="id_kabupaten"]').html(s)
                        }
                    });
                }
            })
            $('select[name="id_kabupaten"]').on('change', function() {
                var id = $(this).val();
                if (IDBFactory) {
                    $.ajax({
                        url: "{{ route('get_kecamatan-by_kabupaten') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        dataType: "json",
                        success: function(data) {
                            console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="id_kecamatan"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '';
                                data.districts.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .name +
                                        '</option>';

                                })
                                $('select[name="id_kecamatan"]').removeAttr('disabled');
                            }
                            $('select[name="id_kecamatan"]').html(s)
                        }
                    });
                }
            })

            $('#pendaftaranForm').on('submit', function(event) {
                event.preventDefault();
                // $("#btnUpdate").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#btnUpdate").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pendaftaran-create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $.toast({
                            icon: 'success',
                            text: data.message,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        $('#ajaxModel').modal('hide');
                        $('#pendaftaranForm').trigger("reset");
                        var oTable = $('.data-table').dataTable();
                        oTable.fnDraw(false);
                        $('#btnUpdate').html('Update Status');
                        $("#btnUpdate").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.changeStatus', function() {
                let id = $(this).data('id');
                let loader = $(this)
                $('#form_result').html('');
                $('#headingStatus').html('Update Status');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pendaftaran-get_by_id') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        // $(loader).html(
                        //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // $(loader).html(
                        //     '<i class="fas fa-info-circle"></i> Sudah jadi');
                        $('#id_status').val(data.id);
                        if (data.status == 0) {
                            $("#pendings").prop("checked", true);
                            $('#sudahJadis').collapse('hide');
                        } else if (data.status == 1) {
                            $("#belums").prop("checked", true);
                            $('#sudahJadis').collapse('hide');
                        } else {
                            $("#sudahs").prop("checked", true);
                            $('#tanggal_jadis').val(data.tanggal_jadi);
                            $('#sudahJadis').collapse('show');
                        }
                        $('#statusModal').modal('show');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this)
                $('#form_result').html('');
                $('#modelHeading').html("Edit Pendaftaran");
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pendaftaran-get_by_id') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="mdi mdi-pencil"></i>');
                        $('#created_at').val(data.created);
                        $('#id_pendaftaran').val(data.id);
                        $('#nomor_perkara').val(data.nomor_perkara);
                        $('#nama').val(data.nama);
                        $('#tahun_perkara').val(data.tahun_perkara);
                        $('#email').val(data.email);
                        $('#jenis_kelamin').val(data.jenis_kelamin);
                        $('#telepon').val(data.hp);
                        $('#provinsi').val(data.id_provinsi).trigger('change');
                        edit_kabupaten(data.id_provinsi, data.id_kabupaten);
                        edit_kecamatan(data.id_kabupaten, data.id_id_kecamatan);
                        $('#id_status').val(data.id);
                        if (data.status == 0) {
                            $("#pending").prop("checked", true);
                            $('#sudahJadi').collapse('hide');
                        } else if (data.status == 1) {
                            $("#belum").prop("checked", true);
                            $('#sudahJadi').collapse('hide');
                        } else {
                            $("#sudah").prop("checked", true);
                            $('#tanggal_jadi').val(data.tanggal_jadi);
                            $('#sudahJadi').collapse('show');
                        }
                        $('#alamat').val(data.alamat);
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $('#StatusForm').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdate").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpdate").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pendaftaran-update_status') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $.toast({
                            icon: 'success',
                            text: data,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        $('#statusModal').modal('hide');
                        $('#StatusForm').trigger("reset");
                        var oTable = $('.data-table').dataTable();
                        oTable.fnDraw(false);
                        $('#btnUpdate').html('Update Status');
                        $("#btnUpdate").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $('#sudahJadi').collapse('hide');
            $('input[name="status"]').change(function() {
                if ($('#sudah').is(":checked")) {
                    $('#sudahJadi').collapse('show');
                } else {
                    $('#sudahJadi').collapse('hide');
                }
            });
            $('#sudahJadis').collapse('hide');
            $('input[name="status"]').change(function() {
                if ($('#sudahs').is(":checked")) {
                    $('#sudahJadis').collapse('show');
                } else {
                    $('#sudahJadis').collapse('hide');
                }
            });

        });

        function edit_kabupaten(id_provinsi, id_kabupaten) {
            $.ajax({
                url: "{{ route('pendaftaran-get_by_kabupaten') }}",
                type: "POST",
                data: {
                    id_provinsi,
                    id_kabupaten
                },
                beforeSend: function() {
                    $('select[name="id_kabupaten"]').append('<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="id_kabupaten"]').html('');
                    $('select[name="id_kabupaten"]').html(fb);
                }
            });
            return false;
        }

        function edit_kecamatan(id_kabupaten, id_kecamatan) {
            $.ajax({
                url: "{{ route('pendaftaran-get_by_kecamatan') }}",
                type: "POST",
                data: {
                    id_kabupaten, id_kecamatan
                },
                beforeSend: function() {
                    $('select[name="id_kecamatan"]').append('<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="id_kecamatan"]').html('');
                    $('select[name="id_kecamatan"]').html(fb);
                }
            });
            return false;
        }
    </script>
@endsection
