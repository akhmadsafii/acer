{{-- <script src="{{ asset('asset/admin/vendors/js/vendor.bundle.base.js') }}"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js">
</script>
<script src="{{ asset('asset/admin/vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('asset/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('asset/admin/vendors/progressbar.js/progressbar.min.js') }}"></script>
<script src="{{ asset('asset/admin/js/off-canvas.js') }}"></script>
<script src="{{ asset('asset/admin/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('asset/admin/js/template.js') }}"></script>
<script src="{{ asset('asset/admin/js/settings.js') }}"></script>
<script src="{{ asset('asset/admin/js/todolist.js') }}"></script>
<script src="{{ asset('asset/admin/js/dashboard.js') }}"></script>
<script src="{{ asset('asset/admin/js/Chart.roundedBarCharts.js') }}"></script>
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

<script>
    function runEffect() {
        // get effect type from
        var selectedEffect = "bounce";

        // Most effect types need no options passed by default
        var options = {};
        // some effects have required parameters
        if (selectedEffect === "scale") {
            options = {
                percent: 50
            };
        } else if (selectedEffect === "size") {
            options = {
                to: {
                    width: 280,
                    height: 185
                }
            };
        }

        // Run the effect
        $("#effect").show(selectedEffect, options, 500, callback);
    };

    //callback function to bring a hidden box back
    function callback() {
        setTimeout(function() {
            $("#effect:visible").removeAttr("style").fadeOut();
        });
    };
</script>
