<!DOCTYPE html>
<html lang="en">

<head>
    @include('content.landing.head')
</head>

<body id="page-top">
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-5 d-flex justify-content-start">
            <img src="{{ asset('asset/pengunjung/img/logo.png') }}" alt="" height="100">
            <div class="title mx-3">
                {{-- <h5 class="my-0" style="color: #286d44">MAHKAMAH AGUNG REPUBLIK INDONESIA</h5> --}}
                <h1 class="font-alt m-0" style="color: #286d44">PENGADILAN AGAMA TEMANGGUNG</h1>
                <p class="m-0" style="color: #286d44">Jl. Pahlawan No.3, Sayangan, Butuh, Kec. Temanggung, Kabupaten
                    Temanggung, Jawa Tengah 56213</p>
            </div>
        </div>
    </nav>
    @yield('content')
    @include('content.landing.footer')
</body>

</html>
