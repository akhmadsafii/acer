@extends('content.landing.main')
@section('content')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <style>
        table.dataTable thead .sorting_asc,
        table.dataTable thead .sorting {
            background: none !important;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #2937f0;
        }

        .slick-prev,
        .slick-next {
            width: 50px;
            height: 50px;
            z-index: 1;
        }

        .slick-prev {
            left: 5px;
        }

        .slick-next {
            right: 5px;
        }

        .slick-prev:before,
        .slick-next:before {
            font-size: 40px;
            text-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px !important;
        }
    </style>
    <div class="container px-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card pt-5 pb-2" style="background-color: #286d44; box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                    <div class="row mx-2">
                        <div class="col-md-8">
                            <div class="slider">
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide1.jpg') }}"
                                            style="max-height: 405px" alt="First slide">
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide2.jpg') }}"
                                            style="max-height: 405px" alt="Second slide">
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide3.jpg') }}"
                                            style="max-height: 405px" alt="Third slide">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card p-2" style="background-color: #ffffff78;">
                                <p class="text-white" style="text-align: justify">Mp4 adalah program dari pengadilan agama
                                    temanggung untuk memberikan kemudahan bagi masyarakan kabupaten
                                    temanggung dalam mengjukan pengambilan akta cerai, <br> dengan adanya sistem ini di
                                    harapkan bisa membantu dalam menganbil akta cerai secara online
                                </p>
                                <p class="text-white text-justify">Best Regards. <br>Pengadilan Agama Temanggung</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 my-1">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-warning p-2">
                                <span class="d-flex justify-content-start"> <i class="fas fa-volume-down fa-2x"></i>
                                    <marquee class="my-auto" style="margin-left: 10px">Selamat Datang di Pendaftaran
                                        Online Pengadilan Agama Temanggung. Silahkan login terlebih dahulu mengajukan
                                        Pendaftaran secara Online.</marquee>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 my-3">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                    Session::forget('success');
                                @endphp
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4 my-3">
                        <div class="card" style="box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                            <div class="card-body" id="mainNav" style="background-color: #ffffff78">
                                <div class="tab-content pt-2">
                                    <div class="tab-pane active" id="datalogin">
                                        <h5 class="font-alt">Form Login</h5>
                                        <hr>
                                        <form action="" method="post">
                                            <div class="form-group my-2">
                                                <label for="">NIK</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Password</label>
                                                <input type="password" class="form-control">
                                            </div>
                                            <div class="form-group my-2">
                                                <div class="aksi-register d-flex justify-content-between">
                                                    <a href="" class="my-auto">Lupa Password</a>
                                                    <button type="submit" class="btn btn-success">Login</button>
                                                </div>
                                            </div>
                                            <div class="form-group my-2 text-center">
                                                <ul class="nav nav-pills nav-fill">
                                                    <li class="nav-item">
                                                        Belum punya akun? <a href="#dataregister" class="nav-link"
                                                            data-toggle="tab" style="display: contents;">Daftar</a>
                                                        sekarang
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="dataregister">
                                        <h5 class="font-alt">Form Register</h5>
                                        <hr>
                                        <div class="alert alert-warning" role="alert" style="text-align: justify">
                                            Harap pastikan Nomor whatsapp anda aktif. Password untuk login akan terkirim
                                            otomatis ke nomor whatsapp oleh sistem
                                        </div>
                                        <form id="form-register">
                                            <div class="form-group my-2">
                                                <label for="">NIK</label>
                                                <input type="text" class="form-control" name="nik">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">NO KK</label>
                                                <input type="text" class="form-control" name="no_kk">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Nama</label>
                                                <input type="text" class="form-control" name="nama">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Nomor Whatsapp</label>
                                                <input type="text" class="form-control" name="telepon">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Email</label>
                                                <input type="text" class="form-control" name="email">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Gender</label>
                                                <select name="jenkel" id="" class="form-control">
                                                    <option value="l">Laki-laki</option>
                                                    <option value="p">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="form-group my-2">
                                                <div class="aksi-register d-flex justify-content-between">
                                                    <div class="form-check my-auto">
                                                        <input type="checkbox" class="form-check-input"
                                                            id="exampleCheck1">
                                                        <label class="form-check-label" for="exampleCheck1">I agree
                                                            the content rules</label>
                                                    </div>
                                                    <button type="submit" class="btn btn-success"
                                                        id="btn-register">Daftar</button>
                                                </div>
                                            </div>
                                            <div class="form-group my-2 text-center">
                                                <ul class="nav nav-pills nav-fill">
                                                    <li class="nav-item">
                                                        Sudah punya akun? <a href="#datalogin" class="nav-link"
                                                            data-toggle="tab" style="display: contents;">Login</a>
                                                        sekarang
                                                    </li>
                                                </ul>
                                                {{-- Sudah punya akun? <a href="#datalogin" class="nav-link"
                                                    data-toggle="tab">Login</a> sekarang --}}
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 my-3">
                        <div class="card" style="box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                            <div class="card-body">
                                <h4 class="font-alt">Riwayat Pendaftar Perkara</h4>
                                <hr>
                                <div class="responsive">
                                    <table class="table table-stripped data-table">
                                        <thead>
                                            <tr>
                                                <th>Nomor Perkara</th>
                                                <th>Jenis Perkara</th>
                                                <th>Nomor Akta Cerai</th>
                                                <th>Tanggal Terbit Akta Cerai</th>
                                                <th>Tanggal Penyerahan AC kepada (P)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.nav-link', function() {
            $('.nav-item .nav-link.active').removeClass('active');
        });

        $('.slider').slick({
            autoplay: true,
            autoplaySpeed: 2500,
            dots: true
        });
        // $(function() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('pengunjung-data_pendaftar') }}",
            columns: [{
                    data: 'nomor_perkara',
                    name: 'nomor_perkara'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                }
            ]
        });

        $('#form-register').on('submit', function(event) {
            event.preventDefault();
            $("#btn-register").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btn-register").attr("disabled", true);
            $.ajax({
                url: "{{ route('register') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#form-register').trigger("reset");
                    }
                    swal(data.status.toUpperCase() + '!', data.message, data.status);
                    $('#btn-register').html('Daftar');
                    $("#btn-register").attr("disabled", false);
                },
                error: function(data) {
                    let resp_error = data.responseJSON;
                    swal(resp_error.status.toUpperCase() + '!', resp_error.message, resp_error.status);
                    $('#btn-register').html('Daftar');
                    $("#btn-register").attr("disabled", false);
                }
            });
        });
    </script>
@endsection
