@extends('content.pengunjung.main')
@section('content')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <style>
        table.dataTable thead .sorting_asc,
        table.dataTable thead .sorting {
            background: none !important;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #2937f0;
        }

        .slick-prev,
        .slick-next {
            width: 50px;
            height: 50px;
            z-index: 1;
        }

        .slick-prev {
            left: 5px;
        }

        .slick-next {
            right: 5px;
        }

        .slick-prev:before,
        .slick-next:before {
            font-size: 40px;
            text-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px !important;
        }
    </style>
    <div class="container px-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card pt-5 pb-2" style="background-color: #286d44; box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                    <div class="row mx-2">
                        <div class="col-md-8">
                            <div class="slider">
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide1.jpg') }}"
                                            style="max-height: 405px" alt="First slide">
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide2.jpg') }}"
                                            style="max-height: 405px" alt="Second slide">
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <img class="d-block w-100"
                                            src="{{ asset('asset/pengunjung/img/slide/slide3.jpg') }}"
                                            style="max-height: 405px" alt="Third slide">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card p-2" style="background-color: #ffffff78;">
                                <p class="text-white" style="text-align: justify">MP4 Adalah Program dari Pengadilan
                                    Agama Temanggung untuk memberikan kemudahan bagi Masyarakat sekitar Temanggung dalam
                                    mendaftarkan/mengajukan perkara Secara
                                    Online.
                                    <br>Dengan adanya sistem ini diharapkan masyarakat bisa terbantu dalam mengurus
                                    perkara
                                </p>
                                <p class="text-white text-justify">Best Regards. <br>Pengadilan Agama Temanggung</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 my-1">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-warning p-2">
                                <span class="d-flex justify-content-start"> <i class="fas fa-volume-down fa-2x"></i>
                                    <marquee class="my-auto" style="margin-left: 10px">Selamat Datang di Pendaftaran
                                        Online Pengadilan Agama Temanggung. Silahkan login terlebih dahulu mengajukan
                                        Pendaftaran secara Online.</marquee>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 my-3">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                    Session::forget('success');
                                @endphp
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4 my-3">
                        <div class="card" style="box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                            <div class="card-body" id="mainNav" style="background-color: #ffffff78">
                                <div class="tab-content pt-2">
                                    <div class="tab-pane active" id="datalogin">
                                        <h5 class="font-alt">Form Login</h5>
                                        <hr>
                                        <form action="" method="post">
                                            <div class="form-group my-2">
                                                <label for="">NIK</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Password</label>
                                                <input type="password" class="form-control">
                                            </div>
                                            <div class="form-group my-2">
                                                <div class="aksi-register d-flex justify-content-between">
                                                    <a href="" class="my-auto">Lupa Password</a>
                                                    <button type="submit" class="btn btn-success">Login</button>
                                                </div>
                                            </div>
                                            <div class="form-group my-2 text-center">
                                                <ul class="nav nav-pills nav-fill">
                                                    <li class="nav-item">
                                                        Belum punya akun? <a href="#dataregister" class="nav-link"
                                                            data-toggle="tab" style="display: contents;">Daftar</a>
                                                        sekarang
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="dataregister">
                                        <h5 class="font-alt">Form Register</h5>
                                        <hr>
                                        <div class="alert alert-warning" role="alert" style="text-align: justify">
                                            Harap pastikan Nomor whatsapp anda aktif. Password untuk login akan terkirim
                                            otomatis ke nomor whatsapp oleh sistem
                                        </div>
                                        <form id="form-register">
                                            <div class="form-group my-2">
                                                <label for="">NIK</label>
                                                <input type="text" class="form-control" name="nik">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">NO KK</label>
                                                <input type="text" class="form-control" name="no_kk">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Nama</label>
                                                <input type="text" class="form-control" name="nama">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Nomor Whatsapp</label>
                                                <input type="text" class="form-control" name="telepon">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Email</label>
                                                <input type="text" class="form-control" name="email">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="">Gender</label>
                                                <select name="jenkel" id="" class="form-control">
                                                    <option value="l">Laki-laki</option>
                                                    <option value="p">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="form-group my-2">
                                                <div class="aksi-register d-flex justify-content-between">
                                                    <div class="form-check my-auto">
                                                        <input type="checkbox" class="form-check-input"
                                                            id="exampleCheck1">
                                                        <label class="form-check-label" for="exampleCheck1">I agree
                                                            the content rules</label>
                                                    </div>
                                                    <button type="submit" class="btn btn-success"
                                                        id="btn-register">Daftar</button>
                                                </div>
                                            </div>
                                            <div class="form-group my-2 text-center">
                                                <ul class="nav nav-pills nav-fill">
                                                    <li class="nav-item">
                                                        Sudah punya akun? <a href="#datalogin" class="nav-link"
                                                            data-toggle="tab" style="display: contents;">Login</a>
                                                        sekarang
                                                    </li>
                                                </ul>
                                                {{-- Sudah punya akun? <a href="#datalogin" class="nav-link"
                                                    data-toggle="tab">Login</a> sekarang --}}
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 my-3">
                        <div class="card" style="box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                            <div class="card-body">
                                <h4 class="font-alt">Riwayat Pendaftar Perkara</h4>
                                <hr>
                                <div class="responsive">
                                    <table class="table table-stripped data-table">
                                        <thead>
                                            <tr>
                                                <th>Nomor Perkara</th>
                                                <th>Jenis Perkara</th>
                                                <th>Nomor Akta Cerai</th>
                                                <th>Tanggal Terbit Akta Cerai</th>
                                                <th>Tanggal Penyerahan AC kepada (P)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="feedbackModal" tabindex="-1" aria-labelledby="feedbackModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
                <div class="modal-header bg-gradient-primary-to-secondary p-4">
                    <h5 class="modal-title font-alt text-white" id="feedbackModalLabel">Daftar Pengajuan</h5>
                    <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body border-0 p-4">
                    <form id="contactForm" data-sb-form-api-token="API_TOKEN" action="{{ route('store_pendaftaran') }}"
                        method="POST">
                        @csrf
                        <div class="form-floating mb-3">
                            <input class="form-control" name="nama" id="name" type="text"
                                placeholder="Enter your name..." data-sb-validations="required" />
                            <label for="name">Nama Lengkap</label>
                            @if ($errors->has('nama'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('nama') }}</div>
                            @endif
                        </div>
                        <!-- Email address input-->
                        <div class="form-floating mb-3">
                            <select name="id_provinsi" id="id_provinsi" class="form-control">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach ($provinsi as $pro)
                                    <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                                @endforeach
                            </select>
                            <label for="email">Pilih Provinsi</label>
                            @if ($errors->has('provinsi'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('provinsi') }}</div>
                            @endif
                        </div>
                        <div class="form-floating mb-3">
                            <select name="id_kabupaten" id="id_kabupaten" class="form-control"></select>
                            <label for="email">Pilih Kabupaten</label>
                        </div>
                        <div class="form-floating mb-3">
                            <select name="id_kecamatan" id="id_kecamatan" class="form-control"></select>
                            <label for="email">Pilih Kecamatan</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input class="form-control" id="email" type="email" placeholder="name@example.com"
                                data-sb-validations="required,email" name="email" />
                            <label for="email">Alamat Email</label>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <!-- Phone number input-->
                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="tel" name="hp"
                                data-sb-validations="required" />
                            <label for="phone">Nomor Telepon</label>
                            @if ($errors->has('hp'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('hp') }}</div>
                            @endif
                        </div>
                        <div class="form-floating mb-3">
                            <select name="jenkel" class="form-control" id="" name="jenkel">
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">perempuan</option>
                            </select>
                            <label for="phone">Jenis Kelamin</label>
                            @if ($errors->has('jenkel'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('jenkel') }}</div>
                            @endif
                        </div>
                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="tel" placeholder=""
                                data-sb-validations="required" name="nomor_perkara" />
                            <label>Nomor Perkara</label>
                            @if ($errors->has('nomor_perkara'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('nomor_perkara') }}</div>
                            @endif
                        </div>
                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="number" placeholder=""
                                data-sb-validations="required" name="tahun_perkara" />
                            <label for="phone">Tahun Perkara</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="tel" placeholder=""
                                data-sb-validations="required" name="alamat" />
                            <label for="phone">Alamat</label>
                        </div>
                        <!-- Submit success message-->
                        <!---->
                        <!-- This is what your users will see when the form-->
                        <!-- has successfully submitted-->
                        <div class="d-none" id="submitSuccessMessage">
                            <div class="text-center mb-3">
                                <div class="fw-bolder">Form submission successful!</div>
                                To activate this form, sign up at
                                <br />
                                <a
                                    href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                            </div>
                        </div>
                        <div class="d-none" id="submitErrorMessage">
                            <div class="text-center text-danger mb-3">Error sending message!</div>
                        </div>
                        <div class="d-grid">
                            <button class="btn btn-primary rounded-pill btn-lg" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.nav-link', function() {
            $('.nav-item .nav-link.active').removeClass('active');
        });

        $('.slider').slick({
            autoplay: true,
            autoplaySpeed: 2500,
            dots: true
        });
        // $(function() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('pengunjung-data_pendaftar') }}",
            columns: [{
                    data: 'nomor_perkara',
                    name: 'nomor_perkara'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                },
                {
                    data: 'nama_kecamatan',
                    name: 'nama_kecamatan'
                }
            ]
        });

        // $('select[name="id_provinsi"]').on('change', function() {
        //     var id = $(this).val();
        //     if (IDBFactory) {
        //         $.ajax({
        //             url: "{{ route('get_kabupaten-by_provinsi') }}",
        //             type: "POST",
        //             data: {
        //                 id
        //             },
        //             dataType: "json",
        //             success: function(data) {
        //                 if (!$.trim(data)) {
        //                     $('select[name="id_kabupaten"]').html(
        //                         '<option value="">--- No Kelas Found ---</option>');
        //                 } else {
        //                     var s = '';
        //                     data.cities.forEach(function(row) {
        //                         s += '<option value="' + row.id + '">' + row
        //                             .name +
        //                             '</option>';

        //                     })
        //                     $('select[name="id_kabupaten"]').removeAttr('disabled');
        //                 }
        //                 $('select[name="id_kabupaten"]').html(s)
        //             }
        //         });
        //     }
        // })
        // $('select[name="id_kabupaten"]').on('change', function() {
        //     var id = $(this).val();
        //     if (IDBFactory) {
        //         $.ajax({
        //             url: "{{ route('get_kecamatan-by_kabupaten') }}",
        //             type: "POST",
        //             data: {
        //                 id
        //             },
        //             dataType: "json",
        //             success: function(data) {
        //                 console.log(data);
        //                 if (!$.trim(data)) {
        //                     $('select[name="id_kecamatan"]').html(
        //                         '<option value="">--- No Kelas Found ---</option>');
        //                 } else {
        //                     var s = '';
        //                     data.districts.forEach(function(row) {
        //                         s += '<option value="' + row.id + '">' + row
        //                             .name +
        //                             '</option>';

        //                     })
        //                     $('select[name="id_kecamatan"]').removeAttr('disabled');
        //                 }
        //                 $('select[name="id_kecamatan"]').html(s)
        //             }
        //         });
        //     }
        // })

        $('#form-register').on('submit', function(event) {
            event.preventDefault();
            $("#btn-register").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btn-register").attr("disabled", true);
            $.ajax({
                url: "{{ route('register') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#form-register').trigger("reset");
                    }
                    swal(data.status.toUpperCase() + '!', data.message, data.status);
                    $('#btn-register').html('Daftar');
                    $("#btn-register").attr("disabled", false);
                },
                error: function(data) {
                    let resp_error = data.responseJSON;
                    swal(resp_error.status.toUpperCase() + '!', resp_error.message, resp_error.status);
                    $('#btn-register').html('Daftar');
                    $("#btn-register").attr("disabled", false);
                }
            });
        });
    </script>
@endsection
