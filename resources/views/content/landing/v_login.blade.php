@extends('content.landing.main')
@section('content')
    <div class="container">
        <div class="row d-flex justify-content-center masthead pt-5 bg-white">
            <div class="col-md-4">
                <div class="card" style="box-shadow: 0 1px 14px 0 rgb(0 0 0);">
                    <div class="card-body py-5">
                        <div class="row">
                            <div class="col-md-12">
                                <center class="my-2">
                                    <i class="fas fa-user-circle fa-7x text-success"></i>
                                    <h5 class="text-success font-alt">LOGIN ADMIN</h5>
                                    <hr>
                                </center>
                            </div>
                            <div class="col-md-12">
                                <form action="http://localhost:9000/auth/verifylogin" method="POST">
                                    <div class="form-group my-2">
                                        <input type="text" class="form-control" id="inputFirstName"
                                            placeholder="Masukan Username">
                                    </div>
                                    <div class="form-group my-2">
                                        <input type="password" class="form-control" id="inputFirstName"
                                            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">
                                    </div>
                                    <div class="col-md-12 d-flex justify-content-between">
                                        <a href="" class="my-auto">Lupa kata sandi</a>
                                        <button type="submit" class="btn btn-success">MASUK</button>
                                    </div>
                                </form>
                                <div class="text-center mt-5">
                                    Kembali ke halaman <a href="{{ url('/') }}" class="text-danger">beranda</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
